#========================================================================
#Gtk Theme Switch, a fast and small Gtk theme switching utility that has a 
#GUI and console interface.
#
#Copyright (C) 2009  Maher Awamy <muhri@muhri.net>
#                    Aaron Lehmann <aaronl@vitelus.com>
#                    Joshua Kwan <joshk@triplehelix.org> 
#                    Pedro Villavicencio Garrido <pvillavi@gnome.cl>
#                    Denis Briand <denis@narcan.fr>
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along
#with this program; if not, write to the Free Software Foundation, Inc.,
#51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#=========================================================================

GCC = gcc
PREFIX=/usr/local
CFLAGS = -g -O2 -Wall
CFLAGS_GTK2 = $(shell pkg-config gtk+-2.0 --cflags)

LIBS_GTK2 = $(shell pkg-config gtk+-2.0 --libs)

VERSION = 2.1.0

all: gtk-theme-switch2

gtk-theme-switch2: main.c
	${GCC} -DSWITCH_GTK2 -o gtk-theme-switch2 main.c ${CFLAGS} ${CFLAGS_GTK2} ${LIBS_GTK2}

clean:
	-rm -f gtk-theme-switch2 *~
	
install: all
	mkdir -p ${PREFIX}/bin
	mkdir -p ${PREFIX}/man
	install -c gtk-theme-switch2 ${PREFIX}/bin
	install -c gtk-theme-switch2.1 ${PREFIX}/man

dist: clean
	rm -rf /tmp/gtk-theme-switch-$(VERSION)
	cp -r . /tmp/gtk-theme-switch-$(VERSION)
	tar czf ../gtk-theme-switch-$(VERSION).tar.gz -C /tmp/ gtk-theme-switch-$(VERSION)
	rm -rf /tmp/gtk-theme-switch-$(VERSION)
